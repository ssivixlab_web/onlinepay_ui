export const environment = {
  production: true,
  endPointUrl: 'https://myclnqapi.ssivixlab.com/',
  redDotURL: 'https://secure-dev.reddotpayment.com/service/Merchant_processor/query_redirection'
};
