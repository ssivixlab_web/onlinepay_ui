import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class LoaderService {
    private isLoading$ = new Subject<boolean>();
    show() {
        this.isLoading$.next(true);
    }
    hide() {
        this.isLoading$.next(false);
    }
    getSpinnerStatus(): Observable<boolean> {
        return this.isLoading$;
    }
}
