import { environment } from 'src/environments/environment';

export const NO_INTERCEPT_LOADER = 'NO__LOADER';
export const PAY_SESSION_KEY = 'MYCLNQ_QUICKPAY_INIT';

export const API_END_POINT_URL = environment.endPointUrl;
export const PAYMENT_API = environment.redDotURL;
export const API_URL = {
    GETPAY_DETAIL: 'pay/request/',
    GETPAYMENT_URL: 'pay/payment/request/',
    UPDATE_PAY_STATUS: 'pay/payment/status',
    PAYMENT_STATUS_CHECK: '/payment_redirect'
};

export const PAYMENT_STATUS = {
    PENDING: 'Pending',
    COMPLETED: 'Completed',
    NO_RESPONSE: 'No Response',
    CANCELLED: 'Cancelled',
    FAILED: 'Failed'
};
