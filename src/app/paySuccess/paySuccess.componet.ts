import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { PAY_SESSION_KEY } from '../constants/constants';
import { QuickPayAPIService } from '../pay/pay.service';

@Component({
  template: `
  <div class="text-center" *ngIf=successTransition>
    <img class="img-fluid" src="assets/paysuccess.png">
    <h2 class="text-success">Payment Success</h2>
    <p> We have Successfully Received the Payment. <a [routerLink]="['/']">Click here</a> to close the window.</p>
    <small>The window will automatically get closed in another {{counter | formatTime}}</small>
</div>
<div class="text-center" *ngIf=errorTransition>
    <img class="img-fluid" src="assets/error.png">
    <h2 class="text-danger">Payment Failue</h2>
    <p>Looks like some issue while processing the payment. Click on the link to retry for the payment
    <a [routerLink]="['/pay/' + payId]">Retry Payment</a></p>
</div>`,
})
export class PaySuccessComponent implements OnInit {
  counter = 8;
  successTransition = false;
  errorTransition = false;
  payId = '';
  constructor(private quickPay: QuickPayAPIService, private route: Router) {
  }

  ngOnInit() {
    let res = JSON.parse(sessionStorage.getItem(PAY_SESSION_KEY));
    if (res && res.init === true && res.payID !== '') {
      // make API Call to server to to check Payment Status
      this.payId = res.payID;
      this.checkPaymentStatus(res);
    }
  }

  private checkPaymentStatus(res) {
    let newPayLoad = {
      ...res.payCheckPayload,
      id: this.payId
    };
    this.quickPay.checkPaymentStatus(newPayLoad).subscribe((paystatus) => {
      if (paystatus.status && paystatus.data === 'Success') {
        this.successTransition = true;
        this.startTimertoCloseWindow();
      } else {
        this.errorTransition = true;
      }
    }, (errStatus) => {
      this.errorTransition = true;
    });
  }

  private startTimertoCloseWindow() {
    sessionStorage.removeItem(PAY_SESSION_KEY);
    timer(0, 1000).subscribe(() => {
      if (this.counter > 0) {
        this.counter--;
      } else if (this.counter === 0) {
        window.close();
        this.route.navigate(['./']);
        this.counter--;
      }
    });
  }

}



@Pipe({
  name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {
  transform(value: number): string {
    // MM:SS format
    const minutes: number = Math.floor(value / 60);
    return (('00' + Math.floor(value - minutes * 60)).slice(-2) + ' Seconds'
    );
  }
}
