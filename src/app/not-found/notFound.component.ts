import { Component, OnInit } from '@angular/core';

@Component({
    template: `
    <div class="notfound">
        <div class="notFound-title mb-3 text-center">404</div>
        <div class="subtext mb-2">Oops! Nothing was found</div>
        <p>The page you are looking for might have been removed had its name changed or
        is temporarily unavailable. <a [routerLink]="['/']">Return to homepage</a></p>
    </div>`,
})
export class NotFoundComponent implements OnInit {

    constructor() { }

    ngOnInit() {

    }



}
