import { Component, OnInit } from '@angular/core';
import { PAY_SESSION_KEY } from '../constants/constants';

@Component({
  template: `
    <h1>WelCome, to My<b>CLNQ</b> Online Payment</h1>
  `,
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    sessionStorage.removeItem(PAY_SESSION_KEY);
  }

}
