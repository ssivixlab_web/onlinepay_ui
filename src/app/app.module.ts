import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoaderModule } from './loader/loader.module';
import { NotFoundComponent } from './not-found/notFound.component';
import { PayComponent } from './pay/pay.component';
import { QuickPayAPIService } from './pay/pay.service';
import { FormatTimePipe, PaySuccessComponent } from './paySuccess/paySuccess.componet';
const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'pay/:id',
    component: PayComponent
  },
  {
    path: 'paySuccess',
    component: PaySuccessComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
@NgModule({
  declarations: [
    HomeComponent,
    AppComponent,
    PayComponent,
    PaySuccessComponent,
    NotFoundComponent,
    FormatTimePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    LoaderModule,
    RouterModule.forRoot(routes, { useHash: true }),
  ],
  providers: [
    QuickPayAPIService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
