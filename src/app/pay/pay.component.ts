import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PAYMENT_STATUS, PAY_SESSION_KEY } from '../constants/constants';
import { QuickPayAPIService } from './pay.service';

@Component({
  templateUrl: './pay.component.html',
})
export class PayComponent implements OnInit {
  title: string = null;
  amoutPreFix: string = 'S$';
  payForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private quickPay: QuickPayAPIService,
    private route: ActivatedRoute) { }

  get isPayBtnDisable(): boolean {
    return this.payForm.get('_id').value === '' || (this.payForm.get('transactionStatus').value !== PAYMENT_STATUS.PENDING);
  }

  ngOnInit() {
    this.createAForm();
    this.route.params.subscribe((res) => {
      this.getData(res.id);
    });
    this.route.queryParams.subscribe(params => {
      if (params && params.status) {
        this.title = 'The Transaction has been Cancelled';
        sessionStorage.removeItem(PAY_SESSION_KEY);
      } else {
        this.title = null;
      }
    });
  }

  private createAForm() {
    this.payForm = this.fb.group({
      paymentDescription: [{ value: '', disabled: true }],
      emailId: [{ value: '', disabled: true }],
      mobileNumber: [{ value: '', disabled: true }],
      amount: [{ value: '', disabled: true }],
      _id: [],
      transactionStatus: []
    });
  }

  private getData(id) {
    this.quickPay.getById(id).subscribe((res) => {
      if (res.status && res.data && res.data[0]) {
        let updateobj = { ...res.data[0] };
        updateobj.amount = `${this.amoutPreFix} ${updateobj.amount}`;
        this.payForm.patchValue(updateobj);
      }
    });
  }

  makePayment() {
    if (this.payForm.get('_id').value !== '') {
      this.quickPay.payRequest(this.payForm.get('_id').value).subscribe((res) => {
        if (res.response_msg === 'successful') {
          let paymentCheckStatus = {
            request_mid: res.mid,
            transaction_id: res.transaction_id,
            signature: res.redirectSignature
          };
          sessionStorage.setItem(PAY_SESSION_KEY, JSON.stringify(
            {
              payID: this.payForm.get('_id').value,
              init: true,
              payCheckPayload: paymentCheckStatus
            }
          ));
          window.location.href = res.payment_url;
        }
      });
    }

  }

}
