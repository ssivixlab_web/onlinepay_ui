import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_END_POINT_URL, API_URL, PAYMENT_API } from '../constants/constants';

@Injectable({
    providedIn: 'root'
})
export class QuickPayAPIService {
    private apiURL = API_END_POINT_URL;
    private payURL = PAYMENT_API;

    constructor(private httpClient: HttpClient) { }

    public getById(id: string): Observable<any> {
        return this.httpClient.get(`${this.apiURL}${API_URL.GETPAY_DETAIL}${id}`);
    }

    public payRequest(payId): Observable<any> {
        return this.httpClient.post(`${this.apiURL}${API_URL.GETPAYMENT_URL}`, { id: payId });
    }

    public checkPaymentStatus(payload): Observable<any> {
        return this.httpClient.post(`${this.apiURL}${API_URL.UPDATE_PAY_STATUS}`, payload);
    }

    // Is Now moved to Node due to CORS ISSUE
    // public checkPaymentStatus(payload): Observable<any> {
    //     let headcr = new HttpHeaders();
    //     headcr = headcr.append('Accept', 'application/json');
    //     return this.httpClient.put(`${this.payURL}${API_URL.PAYMENT_STATUS_CHECK}`, payload, { headers: headcr });
    // }
}
